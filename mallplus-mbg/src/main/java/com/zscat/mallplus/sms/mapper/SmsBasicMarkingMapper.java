package com.zscat.mallplus.sms.mapper;

import com.zscat.mallplus.sms.entity.SmsBasicMarking;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zscat
 * @since 2019-07-07
 */
public interface SmsBasicMarkingMapper extends BaseMapper<SmsBasicMarking> {

}
