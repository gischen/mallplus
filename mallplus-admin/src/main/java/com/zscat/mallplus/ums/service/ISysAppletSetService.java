package com.zscat.mallplus.ums.service;

import com.zscat.mallplus.ums.entity.SysAppletSet;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zscat
 * @since 2019-06-15
 */
public interface ISysAppletSetService extends IService<SysAppletSet> {

}
